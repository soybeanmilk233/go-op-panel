import request from '@/utils/request'

export function getNetList(params) {
  return request({
    url: '/sys/netList',
    method: 'get',
    params
  })
}
