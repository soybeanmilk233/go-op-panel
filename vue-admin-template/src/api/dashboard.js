import request from '@/utils/request'

export function getSystemState(params) {
  return request({
    url: '/sys/systemState',
    method: 'get',
    params
  })
}
