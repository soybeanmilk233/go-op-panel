package models

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB

func NewDB() {
	dsn := "root:123456@tcp(192.168.1.10:3306)/panel?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("[OPEN DB ERROR]:" + err.Error())
	}

	err = db.AutoMigrate(&ConfigBasic{}, &TaskBasic{}, &NetworkBasic{})
	if err != nil {
		panic("[MIGRATE ERROR]:" + err.Error())
	}

	DB = db
}
