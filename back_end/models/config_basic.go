package models

import "gorm.io/gorm"

type ConfigBasic struct {
	gorm.Model
	Keyname string `gorm:"column:keyname; type:varchar(255);" json:"keyname"`
	Value   string `gorm:"column:value; type:varchar(255);" json:"value"`
}

func (table ConfigBasic) TableName() string {
	return "config_basic"
}
