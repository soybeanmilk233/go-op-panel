package models

import (
	"gorm.io/gorm"
)

type NetworkBasic struct {
	gorm.Model
	//名称
	Name string `gorm:"column:name; type:varchar(255);" json:"name"`
	//设备型号
	Type string `gorm:"column:type; type:varchar(255);" json:"type"`
	//厂商
	Brand string `gorm:"column:brand; type:varchar(255);" json:"brand"`
	//管理地址
	Ip string `gorm:"column:ip; type:varchar(255);" json:"ip"`
	//SSH账户
	Account string `gorm:"column:account; type:varchar(255);" json:"account"`
	//密码
	Password string `gorm:"column:password; type:varchar(255);" json:"password"`
	//ping连通状态
	Status string `gorm:"column:status; type:varchar(255);" json:"status"`
}

func (table NetworkBasic) TableName() string {
	return "network_Basic"
}
