package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"op-panel/define"
	"op-panel/middleware"
	"op-panel/models"
	"op-panel/router"
	"op-panel/service"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {

	define.PID = syscall.Getgid()
	fmt.Println("PID:" + string(rune(define.PID)))
	//
	models.NewDB()
	sc := service.GetSystemConfig()
	fmt.Println("--------------Address: http://localhost" + sc.Port + sc.Entry)
	middleware.GeLocalIP()
	ub := service.InitUserConfig()
	fmt.Println("=====UserName: " + ub.Name)
	fmt.Println("=====Password: " + ub.Password)

	//定时任务
	cron := make(chan struct{})
	go service.Cron(cron) //先不使用

	app := router.Router(sc)
	//app.Use(gin.)

	//app.Use(cors.Default())

	srv := &http.Server{
		Addr:    sc.Port,
		Handler: app,
	}

	go func() {
		// 服务连接
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	quit := make(chan os.Signal)
	go func() {
		signal.Notify(quit, os.Interrupt)
	}()
	select {
	case <-quit:
		log.Println(time.Now().Format("2006-01-02 15:04:05") + "Shutdown Server ...")

		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		if err := srv.Shutdown(ctx); err != nil {
			log.Fatal("Server Shutdown:", err)
		}
		cron <- struct{}{}
		main()
		log.Println(time.Now().Format("2006-01-02 15:04:05") + "Server exiting")
	}

}
