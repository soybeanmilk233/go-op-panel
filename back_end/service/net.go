package service

import (
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"op-panel/models"
	"strconv"
)

func NetdList(c *gin.Context) {
	var (
		size, _  = strconv.Atoi(c.DefaultQuery("size", "20"))
		index, _ = strconv.Atoi(c.DefaultQuery("index", "1"))
		tb       = make([]*models.NetworkBasic, 0)
		cnt      int64
	)

	err := models.DB.Model(new(models.NetworkBasic)).Count(&cnt).
		Offset((index - 1) * size).Limit(size).Find(&tb).
		Error
	if err != nil {
		log.Println("[DB ERROR]" + err.Error())
		c.JSON(http.StatusOK, gin.H{
			"code": -1,
			"msg":  "系统异常" + err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg":  "网络设备加载成功",
		"data": gin.H{
			"list":  tb,
			"count": cnt,
		},
	})
	return
}
