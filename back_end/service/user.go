package service

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"log"
	"net/http"
	"op-panel/define"
	"op-panel/helper"
	"op-panel/models"
)

type Loginuser struct {
	Username string `form:"username"    binding:"required" json:"username"`
	Password string `form:"password"  json:"password"  binding:"required"`
	//username string `form:"username"    binding:"required" json:"username"`
	//password string `form:"password"  json:"password"  binding:"required"`
	//Username string `form:"username" json:"username" xml:"username"  binding:"required"`
	//Password string `form:"password" json:"password" xml:"password" binding:"required"`
}

func Login(c *gin.Context) {
	//name := c.PostForm("name")
	//password := c.PostForm("password")
	var lu Loginuser
	if err := c.ShouldBind(&lu); err != nil {
		// 返回错误信息
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	log.Println(lu.Password)
	log.Println(lu.Username)

	if lu.Username == "" || lu.Password == "" {
		c.JSON(http.StatusOK, gin.H{
			"code": -1,
			"msg":  "用户名或密码不能为空",
		})
		return
	}
	cb := new(models.ConfigBasic)
	fmt.Println(models.DB)
	err := models.DB.Model(new(models.ConfigBasic)).Where("keyname = 'user'").First(cb).Error //Where(models.ConfigBasic{Key: "user"}).    //

	if err != nil {
		if err == gorm.ErrRecordNotFound {
			c.JSON(http.StatusOK, gin.H{
				"code": -1,
				"msg":  "用户信息未初始化",
			})
			return
		}
		c.JSON(http.StatusOK, gin.H{
			"code": -1,
			"msg":  "系统异常",
		})
		return
	}

	ub := new(define.UserBasic)
	json.Unmarshal([]byte(cb.Value), ub)
	if ub.Password != lu.Password || ub.Name != lu.Username {
		c.JSON(http.StatusOK, gin.H{
			"code": -1,
			"msg":  "用户或密码不正确",
			//"ub1":   ub.Name,
			//"ub2":   ub.Password,
		})
		return
	}
	token, err := helper.GenerateToken()
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"code": -1,
			"msg":  "系统异常",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"data": gin.H{
			"token": token,
		},
		"msg": "登陆成功",
	})

	return
}
