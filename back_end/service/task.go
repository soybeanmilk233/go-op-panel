package service

import (
	"bufio"
	"errors"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"op-panel/define"
	"op-panel/helper"
	"op-panel/models"
	"os"
	"path"
	"strconv"
	"syscall"
)

func TaskDelete(c *gin.Context) {
	//获取存到的id
	id := c.PostForm("id")
	err := models.DB.Table("task_basic").Where("id = ?", id).Delete(new(models.TaskBasic)).Error
	//err := models.DB.Where("id = ?", id).Delete(new(models.TaskBasic)).Error
	if err != nil {
		log.Println("[ DB ERROR ] : " + err.Error())
		c.JSON(http.StatusOK, gin.H{
			"code": -1,
			"msg":  "系统异常" + err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code":   200,
		"msg":    "删除成功",
		"del_id": id,
	})

	syscall.Kill(define.PID, syscall.SIGINT)
	return
}

func TaskAdd(c *gin.Context) {
	//获取存到的数据
	name := c.PostForm("name")
	spec := c.PostForm("spec")
	data := c.PostForm("data")
	if name == "" || spec == "" || data == "" {
		c.JSON(http.StatusOK, gin.H{
			"code": -1,
			"msg":  "必填参不能为空",
		})
		return
	}

	shellName := helper.GetUUID()
	tb := &models.TaskBasic{
		Name:      name,
		Spec:      spec,
		ShellPath: define.ShellDir + "/" + shellName + ".sh",
		LogPath:   define.LogDir + "/" + shellName + ".log",
	}
	error := models.DB.Create(tb).Error
	if error != nil {
		log.Println("[ DB ERROR ] : " + error.Error())
		c.JSON(http.StatusOK, gin.H{
			"code": -1,
			"msg":  "系统异常" + error.Error(),
		})
		return
	}

	f, err := os.Create(tb.ShellPath)
	if errors.Is(err, os.ErrNotExist) {
		os.MkdirAll(path.Dir(tb.ShellPath), 0777)
		f, err = os.Create(tb.ShellPath)
		if err != nil {
			log.Println("[CREATE FILE ERROR] : " + err.Error())
			c.JSON(http.StatusOK, gin.H{
				"code": -1,
				"msg":  "系统异常" + error.Error(),
			})
			return
		}
	}
	w := bufio.NewWriter(f)
	w.WriteString(data)
	w.Flush()

	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg":  "新增成功",
	})

	syscall.Kill(define.PID, syscall.SIGINT)
	return
}

func TaskEdit(c *gin.Context) {
	id := c.PostForm("id")
	name := c.PostForm("name")
	spec := c.PostForm("spec")
	data := c.PostForm("data")

	if id == "" || name == "" || spec == "" || data == "" {
		c.JSON(http.StatusOK, gin.H{
			"code": -1,
			"msg":  "必填参不能为空",
		})
		return
	}

	tb := new(models.TaskBasic)
	err := models.DB.Where("id = ?", id).First(tb).Error
	if err != nil {
		log.Println("[DB ERRO] " + err.Error())
		c.JSON(http.StatusOK, gin.H{
			"code": -1,
			"msg":  "系统异常 " + err.Error(),
		})
		return
	}

	tb.Name = name
	tb.Spec = spec

	//更新配置
	err = models.DB.Where("id = ?", id).Updates(tb).Error
	if err != nil {
		log.Println("[DB ERRO] " + err.Error())
		c.JSON(http.StatusOK, gin.H{
			"code": -1,
			"msg":  "系统异常 " + err.Error(),
		})
		return
	}

	f, err := os.Create(tb.ShellPath)
	if errors.Is(err, os.ErrNotExist) {
		os.MkdirAll(path.Dir(tb.ShellPath), 0777)
		f, err = os.Create(tb.ShellPath)
		if err != nil {
			log.Println("[CREATE FILE ERROR] : " + err.Error())
			c.JSON(http.StatusOK, gin.H{
				"code": -1,
				"msg":  "系统异常 : " + err.Error(),
			})
			return
		}
	}
	w := bufio.NewWriter(f)
	w.WriteString(data)
	w.Flush()

	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg":  "编辑成功",
	})

	syscall.Kill(define.PID, syscall.SIGINT)
	return
}

func TaskList(c *gin.Context) {
	var (
		size, _  = strconv.Atoi(c.DefaultQuery("size", "20"))
		index, _ = strconv.Atoi(c.DefaultQuery("index", "1"))
		tb       = make([]*models.TaskBasic, 0)
		cnt      int64
	)

	err := models.DB.Model(new(models.TaskBasic)).Count(&cnt).
		Offset((index - 1) * size).Limit(size).Find(&tb).
		Error
	if err != nil {
		log.Println("[DB ERROR]" + err.Error())
		c.JSON(http.StatusOK, gin.H{
			"code": -1,
			"msg":  "系统异常" + err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg":  "加载成功",
		"data": gin.H{
			"list":  tb,
			"count": cnt,
		},
	})
	return
}
