package service

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/disk"
	"github.com/shirou/gopsutil/mem"
	"log"
	"net/http"
	"op-panel/define"
	"op-panel/helper"
	"op-panel/models"
	"syscall"
	"time"
)

func SystemState(c *gin.Context) {
	var (
		cpuPercent      float64
		memUsedPercent  float64
		diskUsed        uint64
		diskUsedPercent float64
		//bytesSent       uint64
		//bytesRecv       uint64
	)
	//net网络代码后续优化
	//netCounters, _ := net.IOCounters(false) //bool为false，返回所有网口的总和
	//
	//netSumStrc := netCounters[0]
	////发送总比特
	//bytesSent = netSumStrc.BytesSent
	////接收总比特
	//bytesRecv = netSumStrc.BytesRecv
	////log.Println("netCounterstrc：", netSumStrc)
	////log.Println("BytesSent:", bytesSent)
	////log.Println("BytesRecv:", bytesRecv)

	//cpu
	cpuPercents, _ := cpu.Percent(time.Second, true)
	for _, percent := range cpuPercents {
		cpuPercent += percent
	}
	cpuPercent /= float64(len(cpuPercents))
	//men
	vms, _ := mem.VirtualMemory()
	memUsedPercent = vms.UsedPercent
	//disk  这里是单纯的每个挂载点的使用率相加，后续优化
	partitons, _ := disk.Partitions(true)
	for _, partiton := range partitons {
		us, _ := disk.Usage(partiton.Mountpoint) //某个挂载点的使用量
		diskUsed += us.Used
	}
	allUsage, _ := disk.Usage("/")
	diskUsedPercent = float64(diskUsed) / float64(allUsage.Total) * 100

	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg":  "请求成功",
		"data": gin.H{
			"cpu_used_percent":  fmt.Sprintf("%.2f", cpuPercent),
			"mem_used_percent":  fmt.Sprintf("%.2f", memUsedPercent),
			"disk_used_percent": fmt.Sprintf("%.2f", diskUsedPercent),
			//"BytesSent":         fmt.Sprintf("%.2f", bytesSent),
			//"BytesRecv":         fmt.Sprintf("%.2f", bytesRecv),
		},
	})
}

func UpdateSystemConfig(c *gin.Context) {
	var (
		port  = c.PostForm("port")
		entry = c.PostForm("entry")
		cb    = new(models.ConfigBasic)
		sc    = new(define.SystemConfig)
	)
	//获取现有配置
	err := models.DB.Table("config_basic").Where("keyname", "system").First(cb).Error
	if err != nil {
		log.Printf("[DB ERRO] : %v\n", err)
		c.JSON(http.StatusOK, gin.H{
			"code": -1,
			"msg":  "系统异常" + err.Error(),
		})
		return
	}
	json.Unmarshal([]byte(cb.Value), sc)

	//设置新的配置
	if port != "" {
		sc.Port = ":" + port
	}
	if entry != "" {
		sc.Entry = "/" + entry
	}
	scByte, _ := json.Marshal(sc)

	//更新配置
	error := models.DB.Table("config_basic").Where("keyname", "system").
		Update("value", scByte).Error
	if error != nil {
		log.Printf("[DB ERRO] : %v\n", err)
		c.JSON(http.StatusOK, gin.H{
			"code": -1,
			"msg":  "系统配置更新失败" + error.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg":  "系统配置修改成功!但还需要重启生效",
	})

	//重启服务--Gin--Windows写法
	syscall.Kill(define.PID, syscall.SIGINT)
	return
}

func GetDefaultSystemConfig() *define.SystemConfig {
	return &define.SystemConfig{
		Port:  ":1888",
		Entry: "/" + helper.RandomString(8),
	}
}

func GetDefaultUserBasic() *define.UserBasic {
	return &define.UserBasic{
		Name:     helper.RandomString(4),
		Password: helper.RandomString(8),
	}
}

func GetSystemConfig() *define.SystemConfig {
	sc := new(define.SystemConfig)
	cb := new(models.ConfigBasic)
	dsc := GetDefaultSystemConfig()
	dscByte, _ := json.Marshal(dsc)
	err := models.DB.Model(new(models.ConfigBasic)).
		Where(models.ConfigBasic{Keyname: "system"}).
		Attrs(map[string]interface{}{"keyname": "system", "value": string(dscByte)}).
		FirstOrCreate(cb).Error
	if err != nil {
		panic("[INIT SYSTEM CONFIG ERROR:" + err.Error())
	}
	err = json.Unmarshal([]byte(cb.Value), sc)
	if err != nil {
		panic("[Unmarshal  ERROR:" + err.Error())

	}
	return sc
}

func InitUserConfig() *define.UserBasic {
	dub := GetDefaultUserBasic()
	dubByte, _ := json.Marshal(dub) //此时的dubByte : { "name": "随机","password": "随机" }
	ub := new(define.UserBasic)
	cb := new(models.ConfigBasic)
	err := models.DB.Model(new(models.ConfigBasic)).
		Where(models.ConfigBasic{Keyname: "user"}).
		Attrs(map[string]interface{}{"keyname": "user", "value": string(dubByte)}). //map[key:user ,value:{,"Name":" ","Password":"123456"}]
		FirstOrCreate(cb).Error
	if err != nil {
		panic("[INIT UserBasic  ERROR:" + err.Error())
	}
	err = json.Unmarshal([]byte(cb.Value), ub) //没错再把"Value":的值返给用户结构体
	if err != nil {
		panic("[Unmarshal User ERROR:" + err.Error())

	}
	return ub
}
