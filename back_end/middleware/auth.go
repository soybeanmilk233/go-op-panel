package middleware

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"op-panel/helper"
)

func Auth() gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.GetHeader("token")
		err := helper.ParseToken(token)
		if err != nil {
			c.JSON(http.StatusOK, gin.H{
				"code":    50014,
				"message": "身份认证不通过",
			})
			c.Abort()
			return
		}
	}
}
