package router

import (
	"github.com/gin-gonic/gin"
	"op-panel/define"
	"op-panel/middleware"
	"op-panel/service"
)

var app *gin.Engine

func Router(sc *define.SystemConfig) *gin.Engine {
	// 简单的路由组: v1

	app = gin.Default()
	//解决跨域
	app.Use(middleware.Cors())
	v1 := app.Group(sc.Entry)
	//v1.Use(cors.Default())

	{
		v1.GET("/", func(c *gin.Context) {
			c.JSON(200, gin.H{
				"message": "success",
			})
		})
		//登陆
		v1.POST("/login", service.Login)

		//需要认证操作的分组
		//v2 := v1.Group("/sys", middleware.Auth())

		v2 := v1.Group("/sys", middleware.Auth())
		//v2.Use(middleware.Auth())
		{
			//修改系统配置
			v2.PUT("/systemConfig", service.UpdateSystemConfig)
			//系统状态
			v2.GET("/systemState", service.SystemState)
			//任务列表
			v2.GET("/taskList", service.TaskList)
			v2.POST("/taskAdd", service.TaskAdd)
			v2.POST("/taskEdit", service.TaskEdit)
			v2.POST("/taskDelete", service.TaskDelete)
			//网络设备
			v2.GET("/netList", service.NetdList)

		}
	}
	return app
}
